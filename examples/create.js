const { io } = require('socket.io-client')
const api = require('@sat-mtl/scenic-api')

const { quiddity: { QuiddityAPI } } = api

const socket = io('ws://localhost:8000', {
  'reconnection delay': 0,
  'reopen delay': 0,
  'force new connection': true,
  autoConnect: false,
  transports: ['websocket']
})

socket.on('connect', async () => {
  const quiddityAPI = new QuiddityAPI(socket)
  const osc = await quiddityAPI.create('OSCsink', 'osc')
  console.log(`quiddity ${osc.id} is created!`)
})

socket.open()
