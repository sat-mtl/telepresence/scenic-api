Hello, welcome in the scenic-api REPL!

Pre-configured 'socket' and 'api' are available **immediatly**:

```js
  // the socket is opened automatically when the REPL is starting!
  await api.quiddityAPI.listCreated()
```

You can still override the default objects by rebuilding a new socket and a new api:

```js
const socket2 = io('ws://localhost:8000', {
  'reconnection delay': 0,
  'reopen delay': 0,
  'force new connection': true,
  autoConnect: false,
  transports: ['websocket']
})

const api2 = scenicApi.default(socket)
```

As exemple, you can connect two quiddities by doing this:

```js
const vid = await api.quiddityAPI.create('videotestsrc', 'vid')
const win = await api.quiddityAPI.create('glfwin', 'win')
await api.quiddityAPI.connect(vid.id, win.id)
await api.propertyAPI.set(vid.id, 'started', true)
```

Happy hacking!
