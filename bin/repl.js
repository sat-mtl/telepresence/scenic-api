#!/usr/bin/env node

;(function () {
  const repl = require('node:repl')
  const fs = require('fs')
  const path = require('path')

  const { io } = require('socket.io-client')
  const scenicAPI = require('@sat-mtl/scenic-api')

  const r = repl.start('node:scenic-api> ')

  const mdPath = path.resolve(__dirname, 'repl.md')
  fs.readFile(mdPath, 'UTF-8', (err, content) => {
    if (err) {
      console.trace(err)
      process.exit()
    } else {
      console.info(content)
      r.displayPrompt()
    }
  })

  r.context.io = io
  r.context.scenicAPI = scenicAPI

  const socket = io('ws://localhost:8000', {
    'reconnection delay': 0,
    'reopen delay': 0,
    'force new connection': true,
    transports: ['websocket']
  })

  socket.on('connect', async () => {
    console.info('the socket is well-connected to swIO!')
    r.displayPrompt()
  })

  r.context.socket = socket
  r.context.api = scenicAPI.default(socket)
})()
