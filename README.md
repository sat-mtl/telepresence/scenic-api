# ScenicAPI

The **Scenic suite** is a telepresence software stack that allows for real-time transmission of audiovisual and arbitrary data over any IP network. Telepresence systems can be used in various artistic contexts, so that two different creative spaces can communicate with each other in real-time and present a combined performance.

*Scenic API* is a wrapper around [socket.io-client](https://www.npmjs.com/package/socket.io-client) in order to handle the [SwitcherIO](https://gitlab.com/sat-mtl/metalab/switcher) API easily.

## Using the REPL

For troubleshooting sessions, a convenient REPL is provided on top of the `scenic-api` and can be used on top of a running `switcher` instance:

```sh
node bin/repl.js
```

Consult the REPL help [here](./bin/repl.md).

## Debugging local changes

When local changes are made, some new features can remain unavailable in a new NodeJS environment. Building and linking the `scenic-api` bundle directly should help:

1. Rebuild the `scenic-api` bundle.

```bash
npm install
npm run build
```

2. Open a NodeJS environment from the local build.

```bash
// you should be in a nodeJS env by using the `node` command
const api = require('./dist/scenic-api.js')
```
