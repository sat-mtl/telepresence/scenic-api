const path = require('path')
const LIFECYCLE_EVENT = process.env.npm_lifecycle_event

const rootPath = path.resolve(__dirname, '..')

const common = {
  entry: path.resolve(rootPath, 'lib/index.js'),
  output: {
    filename: 'scenic-api.js',
    path: path.resolve(rootPath, 'dist'),
    clean: true,
    globalObject: 'this',
    library: {
      name: {
        root: 'ScenicAPI',
        amd: 'scenic-api',
        commonjs: 'scenic-api'
      },
      type: 'umd'
    }
  },
  resolve: {
    alias: {
      '~': `${rootPath}`,
      '@api': `${rootPath}/lib`
    }
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }]
  },
  externals: {
    'socket.io-client': {
      commonjs: 'socket.io-client',
      commonjs2: 'socket.io-client',
      amd: 'socket.io-client',
      root: 'socket.io-client'
    }
  }
}

const development = {
  mode: 'development',
  devtool: 'eval',
  optimization: {
    minimize: false
  }
}

const production = {
  mode: 'production',
  optimization: {
    minimize: true
  }
}

if (LIFECYCLE_EVENT === 'start') {
  module.exports = { ...common, ...development }
} else if (LIFECYCLE_EVENT === 'build') {
  module.exports = { ...common, ...production }
} else {
  module.exports = common
}
