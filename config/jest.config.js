// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

const path = require('path')

module.exports = {
  rootDir: path.resolve(__dirname, '..'),

  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // The directory where Jest should output its coverage files
  coverageDirectory: './coverage/',

  collectCoverageFrom: [
    'lib/**/*.js'
  ],

  coverageThreshold: {
    'lib/': {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80
    }
  },

  // A map from regular expressions to module names that allow to stub out resources with a single module
  moduleNameMapper: {
    '^~(.*)': '<rootDir>/$1',
    '^@api(.*)': '<rootDir>/lib$1',
    '^@test(.*)': '<rootDir>/test/$1',
    '^@fixture(.*)': '<rootDir>/test/fixture$1'
  },

  // An array of regexp pattern strings, matched against all module paths before considered 'visible' to the module loader
  modulePathIgnorePatterns: [
    'node_modules'
  ],

  testTimeout: 5000,

  coverageReporters: [
    'text-summary',
    'html'
  ]
}
