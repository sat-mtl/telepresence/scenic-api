/* global describe it expect beforeAll afterAll beforeEach */

import { createSocket, delay } from '@test/testTools'

import QuiddityAPI from '@api/quiddity/QuiddityAPI'
import PropertyAPI from '@api/quiddity/PropertyAPI'
import SessionAPI from '@api/switcher/SessionAPI'

const videotestsrcKind = 'videotestsrc'
const OSCsinkKind = 'OSCsink'

describe('PropertyAPI', () => {
  const socket = createSocket('ws://localhost', 8000)

  let propertyAPI, sessionAPI, quiddityAPI
  let quiddity, quiddity2

  beforeAll((done) => {
    socket.on('connect', () => {
      sessionAPI = new SessionAPI(socket)
      quiddityAPI = new QuiddityAPI(socket)
      propertyAPI = new PropertyAPI(socket)
      done()
    })

    socket.open()
  })

  beforeEach(async () => {
    await sessionAPI.reset()
    quiddity = await quiddityAPI.create(videotestsrcKind, 'test')
    quiddity2 = await quiddityAPI.create(OSCsinkKind, 'test2')
  })

  afterAll(() => socket.close())

  describe('get', () => {
    it('should get the property value', async () => {
      await expect(propertyAPI.get(quiddity.id, 'resolution'))
        .resolves.toEqual('1')
    })

    it('should get a falsy property value', async () => {
      // it rejects
      await expect(propertyAPI.get(quiddity2.id, 'autostart'))
        .resolves.toEqual(false)
    })
  })

  describe('set', () => {
    it('should set the property value', async () => {
      await expect(propertyAPI.set(quiddity.id, 'resolution', '2'))
        .resolves.toEqual('2')
    })

    it('should set a falsy property value', async () => {
      await expect(propertyAPI.set(quiddity2.id, 'autostart', false))
      // it rejects
        .resolves.toEqual(false)
    })

    it('should set a truthy property value', async () => {
      await expect(propertyAPI.set(quiddity2.id, 'autostart', true))
      // it passes
        .resolves.toEqual(true)
    })
  })

  describe('onUpdated', () => {
    beforeEach(async () => {
      await delay(1000)
    })

    it('should triggered an update when the property is set', (done) => {
      propertyAPI.onUpdated(
        (quidId, propertyName, value, propertyId) => {
          expect(quidId).toEqual(quidId)

          if (propertyName === 'resolution') {
            expect(propertyName).toEqual('resolution')
            expect(value).toEqual('3')
            done()
          }
        }
      )

      propertyAPI.set(quiddity.id, 'resolution', '3')
    })
  })
})
