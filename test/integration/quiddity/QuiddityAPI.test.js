/* global describe it expect beforeAll afterAll beforeEach */

import { createSocket, delay } from '@test/testTools'

import QuiddityAPI from '@api/quiddity/QuiddityAPI'
import SessionAPI from '@api/switcher/SessionAPI'
import PropertyAPI from '@api/quiddity/PropertyAPI'
import UserTreeAPI from '@api/tree/UserTreeAPI'

import switcherKinds from '@fixture/switcher.kinds.json'

describe('QuiddityAPI', () => {
  const OSCsinkKind = switcherKinds.kinds[0].kind
  const socket = createSocket('ws://localhost', 8000)

  let quiddityAPI, sessionAPI, userTreeAPI, propertyAPI

  beforeAll((done) => {
    socket.on('connect', () => {
      quiddityAPI = new QuiddityAPI(socket)
      sessionAPI = new SessionAPI(socket)
      userTreeAPI = new UserTreeAPI(socket)
      propertyAPI = new PropertyAPI(socket)
      done()
    })

    socket.open()
  })

  beforeEach((done) => {
    sessionAPI.clear().then(() => done())
  })

  afterAll(() => socket.close())

  describe('listKinds', () => {
    it('should list all available classes in Switcher', async () => {
      const available = await quiddityAPI.listKinds()
      await expect(available.kinds.length).toBeGreaterThan(0)
    })
  })

  describe('listCreated', () => {
    it('should list all created quiddities', async () => {
      await expect(quiddityAPI.listCreated()).resolves.toStrictEqual([])
    })
  })

  describe('create', () => {
    const port = 1000
    const test = { test: 'test' }

    it('should create a new quiddity', async () => {
      await expect(quiddityAPI.create(OSCsinkKind, 'test')).resolves.toEqual(
        expect.objectContaining({ kind: 'OSCsink' })
      )
    })

    it('should create configured quiddities with default properties and userTree', async () => {
      const quid = await quiddityAPI.create(OSCsinkKind, 'sinkTest', { port }, test)
      await delay(1000)

      await expect(propertyAPI.get(quid.id, 'port')).resolves.toEqual(port)
      await expect(userTreeAPI.get(quid.id)).resolves.toEqual(test)
    })

    it('should get the full quiddity model when created', async () => {
      const quid = await quiddityAPI.create(OSCsinkKind, 'sinkTest', { port }, test)

      expect(quid.id).not.toBeUndefined()
      expect(quid.nickname).not.toBeUndefined()
      expect(quid.connectionSpecs).not.toBeUndefined()
      expect(quid.infoTree.property).not.toBeUndefined()
      expect(quid.userTree).toEqual(test)
    })
  })

  describe('protect', () => {
    it('should protect specific quiddities from a call to reset', async () => {
      // creates a bunch of quids
      const quid1 = await quiddityAPI.create(OSCsinkKind, 'sinkTest1')
      await quiddityAPI.create(OSCsinkKind, 'sinkTest2')
      await quiddityAPI.create(OSCsinkKind, 'sinkTest3')
      const quid4 = await quiddityAPI.create(OSCsinkKind, 'sinkTest4')
      // protects the first and the last one
      await quiddityAPI.protect([quid1.id, quid4.id])
      // reset
      await sessionAPI.reset()
      // there should be two quiddities remaining
      await expect(quiddityAPI.listCreated()).resolves.toHaveLength(2)
    })
    it('should protect all existing quiddities from a call to reset', async () => {
      // creates a bunch of quids
      await quiddityAPI.create(OSCsinkKind, 'sinkTest1')
      await quiddityAPI.create(OSCsinkKind, 'sinkTest2')
      // protects all the already existing quiddities.
      await quiddityAPI.protect()
      await quiddityAPI.create(OSCsinkKind, 'sinkTest3')
      // reset
      await sessionAPI.reset()
      // there should be two quiddities remaining
      await expect(quiddityAPI.listCreated()).resolves.toHaveLength(2)
    })
  })

  describe('onCreated', () => {
    it('should be triggered when a quiddity has been created', (done) => {
      quiddityAPI.onCreated(quid => done())
      quiddityAPI.create(OSCsinkKind, 'test')
    })
  })

  describe('delete', () => {
    let quiddity

    beforeEach(async () => {
      quiddity = await quiddityAPI.create(OSCsinkKind, 'test')
    })

    it('should delete a quiddity', async () => {
      await expect(quiddityAPI.listCreated()).resolves.toHaveLength(1)
      await quiddityAPI.delete(quiddity.id)
      await expect(quiddityAPI.listCreated()).resolves.toHaveLength(0)
    })
  })

  describe('onDeleted', () => {
    let quiddity

    beforeEach(async () => {
      quiddity = await quiddityAPI.create(OSCsinkKind, 'test')
    })

    it('should be triggered when a quiddity has been deleted', (done) => {
      quiddityAPI.onDeleted(quid => done())
      quiddityAPI.delete(quiddity.id)
    })
  })

  describe('connect', () => {
    let src, dst

    it('should connect two compatible quiddities', async () => {
      src = await quiddityAPI.create('OSCsrc', 'src')
      dst = await quiddityAPI.create('OSCsink', 'dst')
      await expect(quiddityAPI.connect(src.id, dst.id)).resolves.toBeDefined()
    })

    it('should fail to connect two incompatible quiddities', async () => {
      src = await quiddityAPI.create('OSCsrc', 'src')
      dst = await quiddityAPI.create('OSCsrc', 'dst')
      await expect(quiddityAPI.connect(src.id, dst.id)).rejects.toBeDefined()
    })
  })

  describe('disconnect', () => {
    let src, dst

    it('should disconnect two connected quiddities', async () => {
      src = await quiddityAPI.create('OSCsrc', 'src')
      dst = await quiddityAPI.create('OSCsink', 'dst')
      const sfid = await quiddityAPI.connect(src.id, dst.id)
      await delay(3000)
      await expect(quiddityAPI.disconnect(dst.id, sfid)).resolves.toBe(true)
    })

    it('should fail to connect two disconnected quiddities', async () => {
      src = await quiddityAPI.create('OSCsrc', 'src')
      dst = await quiddityAPI.create('OSCsink', 'dst')
      await expect(quiddityAPI.disconnect(dst.id, -1))
        .rejects
        .toEqual(expect.stringContaining('Could not disconnect quiddity'))
    })
  })
})
