/* global describe it expect jest beforeEach */

import { Socket, mockSocketCallback } from '@fixture/socket.mock'
import PropertyAPI from '@api/quiddity/PropertyAPI'

describe('PropertyAPI', () => {
  let api, mockEmitCallback

  const QUIDDITY_ID = 'test'
  const PROPERTY_NAME = 'test'
  const PROPERTY_VALUE = {}

  beforeEach(() => {
    api = new PropertyAPI(new Socket())
    // bind created a partial application of the mockSocketCallback with the socket and the method preset
    // to api.socket and the method 'emit' and this bound to null. The mockEmitCallback can then
    // be called with 2 arguments, error and data to simulate an emit callback.
    mockEmitCallback = mockSocketCallback.bind(null, api.socket, 'emit')
  })

  describe('constructor', () => {
    it('should PropertyAPI to be defined', () => {
      expect(api).toBeDefined()
      expect(api.socket).toBeDefined()
    })
  })

  describe('get', () => {
    const API_SUCCESS = PROPERTY_VALUE
    const API_ERROR = new Error('Get failure')

    it('should emit `property.get`', async () => {
      await api.get(QUIDDITY_ID, PROPERTY_NAME)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'property.get',
        QUIDDITY_ID,
        PROPERTY_NAME,
        expect.any(Function)
      )
    })

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitCallback(null, API_SUCCESS)
      expect(await api.get()).toEqual(API_SUCCESS)
      // null is not a special value and should not be treated differently
      mockEmitCallback(null, null)
      expect(await api.get()).toEqual(null)
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitCallback(API_ERROR)
      await expect(api.get()).rejects.toEqual(API_ERROR)
    })
  })

  describe('set', () => {
    const API_SUCCESS = PROPERTY_VALUE
    const API_ERROR = new Error('Get failure')

    it('should emit `property.set`', async () => {
      await api.set(QUIDDITY_ID, PROPERTY_NAME, PROPERTY_VALUE)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'property.set',
        QUIDDITY_ID,
        PROPERTY_NAME,
        PROPERTY_VALUE,
        expect.any(Function)
      )
    })

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitCallback(null, API_SUCCESS)
      expect(await api.set()).toEqual(API_SUCCESS)
      // null is not a special value and should not be treated differently
      mockEmitCallback(null, null)
      expect(await api.set()).toEqual(null)
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitCallback(API_ERROR)
      await expect(api.set()).rejects.toEqual(API_ERROR)
    })
  })
  describe('onUpdated', () => {
    const MOCK_UPDATE = jest.fn()

    beforeEach(() => {
      MOCK_UPDATE.mockClear()
    })

    const execOnUpdate = (quiddityMatcher, propertyMatcher) => {
      api.onUpdated(MOCK_UPDATE, quiddityMatcher, propertyMatcher)
      api.socket.onEmit('property.updated', QUIDDITY_ID, PROPERTY_NAME, PROPERTY_VALUE)
    }

    it('should not trigger an update when there is no match', () => {
      execOnUpdate('FAILURE', undefined)
      execOnUpdate(undefined, 'FAILURE')
      execOnUpdate(() => false, undefined)
      execOnUpdate(undefined, () => false)
      expect(MOCK_UPDATE).not.toHaveBeenCalled()
    })

    it('should trigger an update when the matching strings are accepted', () => {
      execOnUpdate(QUIDDITY_ID, PROPERTY_NAME)
      expect(MOCK_UPDATE).toHaveBeenCalledWith(QUIDDITY_ID, PROPERTY_NAME, PROPERTY_VALUE, undefined)
    })

    it('should trigger an update when the matching function are accepted', () => {
      execOnUpdate(() => true, () => true)
      expect(MOCK_UPDATE).toHaveBeenCalledWith(QUIDDITY_ID, PROPERTY_NAME, PROPERTY_VALUE, undefined)
    })

    it('should trigger an update when there is no matcher', () => {
      execOnUpdate()
      expect(MOCK_UPDATE).toHaveBeenCalledWith(QUIDDITY_ID, PROPERTY_NAME, PROPERTY_VALUE, undefined)
    })
  })
})
