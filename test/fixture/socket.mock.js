/* global jest */

/** @classdesc Mocks Socket.IO api */
export function Socket () {
  /** @property Socket id */
  this.id = 'mockSocket'

  /**
   * Mocks Socket.IO emit method
   * @method emit
   */
  this.emit = jest.fn((...args) => {
    const command = args[0]
    const callback = args[args.length - 1]

    switch (command) {
      case 'switcher.kinds':
        callback(null, ['quid1', 'quid2'])
        break

      case 'quiddity.tree.grafted':
      case 'quiddity.userData.grafted':
        callback(args[1], args[2], args[3])
        break

      case 'quiddity.created':
      case 'quiddity.removed':
        callback(args[1])
        break

      case 'quiddity.tree.pruned':
      case 'quiddity.userData.pruned':
        callback(args[1], args[2])
        break

      case 'quiddity.create':
      case 'quiddity.tree.query':
      case 'preview.subscribe':
      case 'preview.unsubscribe':
      default:
        callback(null, true)
        break
    }
  })

  this.commands = {}

  /**
   * Mocks Socket.IO on method
   * @method on
   */
  this.on = jest.fn((onCommand, callback) => {
    if (this.commands[onCommand]) {
      this.commands[onCommand].push(callback)
    } else {
      this.commands[onCommand] = [callback]
    }
  })

  /**
   * Triggers Socket.IO on method
   * @method onEmit
   */
  this.onEmit = (onCommand, arg1, arg2, arg3) => {
    this.commands[onCommand].forEach(callback => {
      callback(arg1, arg2, arg3)
    })
  }

  this.disconnect = () => {}
}

export function SocketWithErrors () {
  this.emit = jest.fn((...args) => {
    const callback = args[args.length - 1]
    callback(new Error('Failed socket :('))
  })
}

/**
 * Mock the socket callback with error and data parameter
 * @param {Socket} socket - The used socket (which is generally mocked here)
 * @param {string} fn - The name of the socket function (emit or on)
 * @param {Error} error - The error parameter (not null means that the socket call fail)
 * @param {(string|number)} data - The resolved data of the socket (means that the socket call success)
 */
export function mockSocketCallback (socket, fn, error, data) {
  socket[fn] = jest.fn((...args) => args[args.length - 1](error, data))
}
