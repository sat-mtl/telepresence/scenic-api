Release Notes
===================

scenic-api 0.7.0 (2023-08-21)
---------------------------------

 * ✨ Force registration with destination port 9060 ([!38](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/38))

scenic-api 0.6.0 (2023-06-29)
---------------------------------

 * ✨ Add features required for thumbnails ([!35](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/35))

scenic-api 0.5.0 (2023-03-02)
---------------------------------

 * ✨ Adds protect method to the quiddity api ([!32](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/32))

scenic-api 0.4.0 (2023-02-21)
---------------------------------

* 🐛 Fix get and set methods of the PropertyAPI reporting failures for all falsy values returned by the api. ([!29](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/29))
* ✨ Add a prune route for the UserTreeApi ([!18](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/18))

scenic-api 0.3.1 (2023-01-04)
----------------------------------

* ⬆ Upgrade socketIO to 4.5.3 ([!21](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/21))

scenic-api 0.3.0 (2022-11-21)
----------------------------------

* 🐛 Fix session load read write ([!17](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/17))
* ✅ Add integration tests for complex values grafting in the userTreeAPI ([!15](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/15))
* 🐛 Provide the quiddity Id to the onUpdated function ([!16](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/16))

scenic-api 0.2.0 (2022-09-30)
----------------------------------

* ✨ Add the session api ([!8](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/8))
* 📝 Update the gitlab templates ([!12](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/12))
* 🔨 Add a REPL mode on top of the scenic-api ([!11](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/11))
* ✨ Update and test the MethodAPI ([!9](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/9))
* 🐛 Fix examples and integration tests ([!10](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/10))

scenic-api 0.1.0 (2022-08-22)
----------------------------------

* ✅ Add sip registration/unregistration api integration ([!4](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/4))
* ✅ Update unit tests ([!3](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/3))
* ✅ Resolve integration of the connection API ([!2](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/2))
* 📝 Add an example usage ([!1](https://gitlab.com/sat-mtl/tools/scenic/scenic-api/-/merge_requests/1))

scenic-api 0.0.0 (2022-08-17)
----------------------------------

**Warning**: this package is still in working progress

* 📦 add ci recipes to release the scenic-api package
* 🤡 remove mocked apis
* ✅ resolve most of the integration tests
* ⚡ reduce the bundle size
* 🐛 package dependencies
* 💚 add ci recipes that tests scenic-api
* 🚨 apply standard linter
* 🔨 add npm scripts to publish
* 🐛 fix basic export
* 🔧 add babel and webpack config
* 📄 add contribution docs
* 🎉 move api parts from scenic